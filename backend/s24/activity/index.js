// console.log("Hello World");


//Objective 1
//Add code here
    
function numberLooper(number) {
  let message = "";

  for (let count = number; count >= 0; count--) {
    if (count <= 50) {
      message = "The current value is at " + count + ". Terminating the loop.";
      break;
    }

    if (count % 10 === 0) {
      console.log("The number is divisible by 10. Skipping the number.");
      continue;
    }

    if (count % 5 === 0) {
      console.log(count);
    }
  }

  return message;
}



//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here
for (let i = 0; i < string.length; i++) {
  let letter = string[i];

  // Step 11: Check if the letter is a vowel
  if (
    letter === "a" ||
    letter === "e" ||
    letter === "i" ||
    letter === "o" ||
    letter === "u"
  ) {
    continue;
  } else {
    
      filteredString += letter;

  }

}

console.log(filteredString);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
        numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null
    }
} catch(err){

}